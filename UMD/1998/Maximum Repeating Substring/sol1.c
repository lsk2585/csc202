#include <stdio.h>
#include "../../../lib/acctools.c"


int main(){
        
        char input_string[50]; //input character array

        int length_input = 0; //tracks length of input 

        int arr_index_repeats[50]; //stores index of repeating substrings first character

        int len_arr_repeats = 0; //stores the number of times a single character repeats

        int num_reps[50]; //stores the amount of times each individual substring repeats

        int num_reps_length; //stores the length of the num_reps array

        int incrementer = 1; //checks for substring greater than 1 character

        int repeat_count = 0; // stores the number of times a substring repeats

        int index_num_reps = 0; //index of the number of repeats of each substring array

        int j = 0; //general loop counter

        int i = 0; //general loop counter

        int biggest_num_reps = 0; //stores index of num_reps which is greatest

        int input_index = 0; //tracks what charcter is being evaluated 

        accgetline(input_string, 50); //gets input

        //determines input string length
        while (input_string[input_index] != '\0'){
                length_input++;
                input_index++;
        }

        input_index = 0;
        
        while (input_string[input_index] != '\0'){
 
                repeat_count = 0;
                len_arr_repeats = 0;
                j = 0;
                //finds index of repeated charcters and puts index into list
                for (int i = 0; i < length_input; i++){
                        if (input_string[input_index] == input_string[input_index+i]){
                                arr_index_repeats[j] = input_index+i;
                                j++;
                        }

                }
                
                incrementer = 1;
                
               //if a character is repeated more than once
                if (j-1 != 0){
                        len_arr_repeats = j; //stores length of array of matching character indecies
                        j = 0; //index of arr_index_repeats
                        
                        //determines length of substring, and number of times it repeats
                        while (j < len_arr_repeats-1){

                                //tests if next consecutive character of two matching characters are the same, if so check next consecuitve character charcter
                                if (input_string[arr_index_repeats[j]] == input_string[arr_index_repeats[j] + incrementer]){
                                                j++;   
                                                incrementer = 1;                        
                                }
                                
                                
                                //tests if next consecuitve charcter is the same as the orginal matching character, if so move onto the index of next matching character
                                else if  (input_string[arr_index_repeats[j] + incrementer] == input_string[arr_index_repeats[j+1] + incrementer]){
                                                incrementer++;
                                                
                                }
                                //if next consecuitve character is not the same as the original chacter, and the next consecutive charcters of the two don't match, this character is not part of substring
                                else {
                                        arr_index_repeats[j] = -1; //this character is not a conseucitve substring
                                        j++;         
                                        incrementer = 1;
                                
                                }
                
                        }
                       
                } 
                
                //counts number of times a substring consecuitvely repeats based based off of arr_index_repeats

                for (j = 0; j < len_arr_repeats; j++){
                        if (arr_index_repeats[j] != -1){
                                repeat_count++;
                        }  
                }

                //printf("%d \n", repeat_count);
                
                num_reps[index_num_reps] = repeat_count; //adds the number of times a substring repeat to list of number of times multiple substrings repeat
                index_num_reps++; 
                len_arr_repeats = index_num_reps; 

               length_input--; //decrease the number of indecies that are evaluated for a matching character
               input_index++;

        }
        

        //determines what the biggest number of repeats is from a list of the number of times a substring repeats
        biggest_num_reps = num_reps[0];
        for (j = 0; j < len_arr_repeats; j++) {
                if (biggest_num_reps < num_reps[j]) {
                        biggest_num_reps = num_reps[j];
                }
        }
        

        printf("%d", biggest_num_reps);
        

        return 0;
}
