from playwright.sync_api import sync_playwright
from bs4 import BeautifulSoup
from csv import writer


#import markdown
import time
import os

# Import the aps staff list and store it on a file

# Read the list with all the links
aps_list_path = os.getcwd() + "/staff_listing.html"
aps_list_html_page = BeautifulSoup(open(aps_list_path, encoding="utf8"), "html.parser")
# All the links to aps websites
all_links_aps = []
for a in aps_list_html_page.find_all('a', href=True):
    all_links_aps.append(a['href'])
# 
with sync_playwright() as p:
        # Launch the browser
        browser = p.chromium.launch()
        # Open a new browser page
        page = browser.new_page()
        # URL to go to a website
        url = "https://careercenter.apsva.us/staff-directory/"
        # Open our webpage
        page.goto(url)
        page_content = page.content()
        soup = BeautifulSoup(page_content, features="html.parser")
        start_content = soup.find("section", id="sd-content")
        staff_listing = start_content.find("div", class_ = "listings")
        all_staff = staff_listing.find_all("div", class_ = "row")
        with open('staff.csv', 'w', encoding='utf8', newline='') as f:
            thewriter = writer(f)
            header =["name", 'position', 'team', 'courses', 'email']
            thewriter.writerow(header)

            for staff in all_staff:
                name = staff.find("div", class_ = "name").text
                position = staff.find("div", class_ = "title").text
                team = staff.find("div", class_ = "team").text
                courses = staff.find("div", class_= "courses").text
                email = staff.find("a",class_="notranslate").text
                teacher = [name, position, team, courses, email]
                thewriter.writerow(teacher)
                
