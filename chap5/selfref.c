#include <stdio.h>

struct node{
    int val;
    struct node *next;
};


int main(){
    struct node point1;
    point1.val = 1;

    struct node point2;
    point2.val = 3;

    point1.next = &point2;

    printf("%d", point1.next->val); //prints 3

    return 0;
}