#include <stdio.h>

typedef union{
        int rad;
}COMPONENT;

typedef struct{
    COMPONENT radius;
}CIRCLE;


int find_area(CIRCLE circle);

int main(){
    CIRCLE circle;
    circle.radius.rad = 5;

    printf("%d", find_area(circle));

    return 0;
}

int find_area(CIRCLE circle){
    int area = 0;
    float PI = 3.14;
    area = 2*(PI)*((circle.radius.rad)*(circle.radius.rad));
    return area;
}