#include <stdio.h>
#include <string.h>

    typedef struct{

        char first_name[10];
        char mid_name[10];
        char last_name[10];

    }NAME;
    
    
    typedef struct{

        int Roll_Number;
        char gender;
        NAME name;
        struct BIRTH_DATE{
            int month;
            int day;
            int year;
        } DOB;

    }STUDENT;

int main(){

    STUDENT stud1;
    stud1.Roll_Number = 1;
    strcpy(stud1.name.first_name, "John");
    strcpy(stud1.name.mid_name, "Adam");
    strcpy(stud1.name.last_name, "Smith");

    printf("%d\n", stud1.Roll_Number);
    printf("%s ", stud1.name.first_name);
    printf("%s ", stud1.name.mid_name);
    printf("%s", stud1.name.last_name);

    return 0;
}