#include <stdio.h>
#include <stdlib.h>


typedef struct{
    int x;
    int y;
}POINT;

int quadrant = 0; 
int main(){

    POINT point = {-1, -5};

    if (point.x > 0) {
        
        if (point.y > 0){
            quadrant = 1;
        }

        else{
            quadrant = 4;
        }
    }

    else{

        if (point.y > 0){
            quadrant = 2;
        }

        else{
            quadrant = 3;
        }
    }

    printf("%d", quadrant);

    return 0;
}