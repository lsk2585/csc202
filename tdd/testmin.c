#include <stdio.h>
#include "minunit.h"
#include "min.c"

int tests_run = 0;
int a[] = {4, 5, 2, 10, 9};
int b[] = {1, 7, 4, 2, 14};
int c[] = {1, -1, 4, 16, 13, 21, 25, 34};

static char * test_min_01(){
    mu_assert("error, min([4, 5, 2, 10, 9], 5) != 2", min(a, 5) == 2);
    return 0;
}

static char * test_min_02(){
    mu_assert("error, min([1, 7, 4, 2, 14], 5) != 1", min(b, 5) == 1);
    return 0;
}

static char * test_min_03(){
    mu_assert("error, min([1, -1, 4, 16, 13, 21, 25, 34], 8) != -1", min(c, 8) == -1);
    return 0;
}

static char * all_tests(){
    mu_run_test(test_min_01);
    mu_run_test(test_min_02);
    mu_run_test(test_min_03);
    return 0;

}
int main (int argc, char **argv) {
    char *result = all_tests();

    if(result !=0) {
        printf("%s\n", result);
    }
    else {
        printf("All tests passed\n");
    }
    printf("Tests run: %d\n", tests_run);
    return result != 0;
}
