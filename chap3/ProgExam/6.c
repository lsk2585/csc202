#include <stdio.h>
/* Write a program to read and display a square (using functions). */

int printvertically(int length){
    for (int i = 0; i < length; i++){
        printf("|");
        for (int i = 0; i < length; i++){
            printf(" ");   
        }
        printf("|");
        printf("\n");
    }
    return 0;
}

int printhorizontaly(int length){
    printf(" ");
    for (int i = 0; i < length; i++)
        printf("-");
    printf(" ");
    return 0;
}

int main(){ 
    int length = 6;
    printhorizontaly(length);
    printf("\n");
    printvertically(length);
    printhorizontaly(length);
    
    return 0;
}