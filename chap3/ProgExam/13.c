#include <stdio.h>
/* Write a program using pointers to interchange the second biggest
 and the second smallest number in the array*/

 int main(){
    int i = 0;
    int array[] = {1,2,3,4,5,6,7,8,9};
    int small, big = array[0];

    
    while (small < array[i]){
        small = array[i];
        i++;
    }
    printf("%d\n",small);
    i = 0;
    while (big > array[i] || big == array[i]){
        big = array[i];
        i++;
    }
    printf("%d",big);
     return 0;
 }