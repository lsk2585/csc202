#include <stdio.h>
#include "../../../minunit.h"
#include "31.c"

int tests_run = 0;
int testInput[] = {1,2,3,4,5};
int testInput[] = {8,6,4,12,9,4};

int i = 0;
char text[] = "error testInput[] =! input[]";
static char * test_min_01(){
        mu_assert("error, min([1, 2, 3, 4, 5], 5) != 2", min(a, 5) == 2);
        return 0;
    }

static char * test_min_02(){
        mu_assert("error, min([4, 5, 2, 10, 9], 5) != 2", min(a, 5) == 2);
        return 0;
    }

static char * all_tests(){
    mu_run_test(test_min_01);
    mu_run_test(test_min_02);
    return 0;
}

int main (int argc, char **argv) {
    char *result = all_tests();

    if(result !=0) {
        printf("%s\n", result);
    }
    else {
        printf("All tests passed\n");
    }
    printf("Tests run: %d\n", tests_run);
    return result != 0;
}
