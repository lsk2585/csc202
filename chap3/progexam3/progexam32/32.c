int mean(int arr[], int len){
    float mean = 0;
    int sum = 0;
    int i =0;
    if (arr[0] == 0) {
        printf("\n Enter the number of elements in the array : ");
        scanf("%d", &len);
        printf("\n Enter elements into the array : ");
        for(i=0;i<len;i++){
            printf("\n arr[%d] = ", i);
            scanf("%d", &arr[i]);
            }
    }

    for(i=0;i<len;i++)
         sum += arr[i];

    mean = (float) sum/len;

    printf("\n The sum of the array elements = %d", sum);
    printf("\n The mean of the array elements = %.2f", mean);

    return mean;

}