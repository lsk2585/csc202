#include <stdio.h>
#include "../../../minunit.h"
#include "32.c"

int tests_run = 0;
int nums1[] = {9,9,9,9,9};
int nums2[] = {8,6,3,9,10,12};
int nums3[20];

static char * test_min_01(){
    mu_assert("error, mean([9, 9, 9, 9, 9], 5) != 9", mean(nums1, 5) == 9);
    return 0;
}

static char * test_min_02(){
    mu_assert("error, mean([8, 6, 3, 9, 10, 12], 6) != 8", mean(nums2, 6) == 8);
    return 0;
}

static char * test_min_03(){
    mu_assert("error, mean(nums3[], 5) =! 9", mean(nums3, 20) == 9);
    return 0;
}


static char * all_tests(){
    mu_run_test(test_min_01);
    mu_run_test(test_min_02);
    mu_run_test(test_min_03);
    return 0;

}




int main (int argc, char **argv) {
    char *result = all_tests();

    if(result !=0) {
        printf("%s\n", result);
    }
    else {
        printf("\nAll tests passed\n");
    }
    printf("Tests run: %d\n", tests_run);
    return result != 0;
}
