#include <stdio.h>
#include "../../../minunit.h"
#include "33.c"

int tests_run = 0;
int a[] = {1,2,3,4,5};
int b[] = {8,6,3,9,10,12};
int c[20];
static char * test_min_01(){
    mu_assert("error, mean([1, 2, 3, 4, 5], 5) != 9", min(a, 5) == 1);
    return 0;
}

static char * test_min_02(){
    mu_assert("error, mean([8, 6, 3, 9, 10, 12], 6) != 3", min(b, 6) == 3);
    return 0;
}

static char * test_min_03(){
    mu_assert("error, mean([], 6) != 3", min(c, 20) == 3);
    return 0;
}

static char * all_tests(){
    mu_run_test(test_min_01);
    mu_run_test(test_min_02);
    mu_run_test(test_min_03);
    return 0;

}


int main (int argc, char **argv) {
    char *result = all_tests();

    if(result !=0) {
        printf("%s\n", result);
    }
    else {
        printf("All tests passed\n");
    }
    printf("Tests run: %d\n", tests_run);
    return result != 0;
}
