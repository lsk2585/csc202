#include <stdio.h>

int min(int arr[], int len){
    int i, small, pos;
    if (arr[0] == 0) {
        printf("\n Enter the number of elements in the array : ");
        scanf("%d", &len);
        printf("\n Enter the elements : ");
        for(i=0;i<len;i++){
            printf("\n arr[%d] = ", i);
            scanf("%d",&arr[i]);
        }
    }
    small = arr[0];
    
    pos = 0;
    for(i=1;i<len;i++){
         if(arr[i]<small){
            small = arr[i];
            pos = i; 
        }
    }
    return small;
}