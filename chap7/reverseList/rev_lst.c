#include "../StackLL.h"

struct node *stack = NULL;

int main() {
    int data[2] = {0,-1};

    int list_to_reverse[10] = {1,2,3,4,5,6,7,8,9,10};
    
    stack = create_stack(stack, data);

    for (int i = 0; i < 10; i++) {
        stack = push(stack, list_to_reverse[i]);
    }

    stack = display(stack);

    return 0;
}