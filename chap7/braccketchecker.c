#include <limits.h>
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#define MAX 10

struct Stack {
    int top;
    unsigned capacity;
    int* array;
};

struct Stack* createStack(unsigned capacity){
    struct Stack* stack = (struct Stack*)malloc(sizeof(struct Stack));
    stack->capacity = capacity;
    stack->top = -1;
    stack->array = (int*)malloc(stack->capacity * sizeof(int));
    return stack;
}

int isFull(struct Stack* stack){
    return stack->top == stack->capacity -1;
}

int isEmpty(struct Stack* stack){
    return stack->top == -1;
}

void push(struct Stack* stack, char item){
    if (isFull(stack))
        return;
    stack->array[++stack->top] = item;
    printf("%c pushed to stack\n", item);
}

int pop(struct Stack* stack){
    if (isEmpty(stack))
            return 0;
        return stack->array[stack->top--];

    
}

int peek(struct Stack* stack){
    if (isEmpty(stack))
        return 0;
    return stack->array[stack->top];

}



int main(){
    char exp[MAX];
    
    struct Stack* stack = createStack(100);
    printf("Enter an expression : ");

    gets(exp);

    for (int i=0; i < (strlen(exp)); i++){
        if (exp[i] == '{' || exp[i] == '[' || exp[i] == '('){
            push(stack, exp[i]);
        }

        if (peek(stack) == '{' && i+1 == strlen(exp) && exp[i] != '}' || peek(stack) == '[' && i + 1 == strlen(exp) && exp[i] != ']' || peek(stack) == '(' && i + 1 == strlen(exp) && exp[i] != ')'){
            printf("Invalid expression");
        }

        if (exp[i] == '}' || exp[i] == ']' || exp[i] == ')'){
            if (stack -> top ==  -1){
                printf("Invalid Expression");
            }

            else {
                char temp = pop(stack);
                if ((temp == '{' && exp[i] != '}') ||(temp == '(' && exp[i] != ')') || (temp == '[' && exp[i] != ']')){
                    printf("invalid expression");
                }
                
            }
        

        }


    }

    if (stack -> top != -1){
        printf("Invalid Expression");
    }

    return 0;
}