#include <limits.h>
#include <stdio.h> 
#include <stdlib.h>

typedef struct node{
    int val;
    struct node *next;
}node;

struct Stack {
    int top;
    unsigned capacity;
    node* array;
};

struct Stack* createStack(unsigned capacity){
    struct Stack* stack = (struct Stack*)malloc(sizeof(struct Stack));
    stack->capacity = capacity;
    stack->top = -1;
    stack->array = (node*)malloc(stack->capacity * sizeof(int));
    return stack;
}

int isFull(struct Stack* stack){
    return stack->top == stack->capacity -1;
}

int isEmpty(struct Stack* stack){
    return stack->top == -1;
}

void push(struct Stack* stack, node item){
    if (isFull(stack))
        return;
    stack->array[++stack->top] = item;
    printf("%d pushed to stack\n", item.val);
}

int pop(struct Stack* stack){
    if (isEmpty(stack)){
        if (isEmpty(stack))
            return INT_MIN;
        return stack->array[stack->top--].val;
    }
}

int peek(struct Stack* stack){
    if (isEmpty(stack))
        return INT_MIN;
    return stack->array[stack->top].val;
}

int main(){
    struct Stack* stack = createStack(100);

    node *start = NULL;
    
    node point1;
    point1.val = 1;

    start = &point1;

    node point2;
    point2.val = 2;

    point1.next = &point2;

    node point3;
    point3.val = 3;

    point2.next = &point3;

    node point4;
    point4.val = 4;

    point3.next = &point4;
    point4.next = NULL;
    
    
    start = &point1;
    
    push(stack, point1);
    push(stack, point2);
    push(stack, point3);
    push(stack, point4);


    printf("%d popped from stack\n", pop(stack));

    

    return 0;
}