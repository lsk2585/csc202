#include <stdio.h>
#include <stdlib.h>

typedef struct node{
    int val;
    struct node *next;
}node;

void reverse_list(node *start);


int main(){
    node *start = NULL;
    
    node point1;
    point1.val = 1;

    start = &point1;

    node point2;
    point2.val = 2;

    point1.next = &point2;

    node point3;
    point3.val = 3;

    point2.next = &point3;

    node point4;
    point4.val = 4;

    point3.next = &point4;
    

    node point5;
    point5.val = 5;
    point4.next = &point5;
    point5.next = NULL;

    
    
    start = &point1;
    printf("\nlinked list: ");

    while (start -> next != NULL){
        printf("current val: %d ", start -> val);
        start = start -> next;
    }

    printf("current val: %d ", start -> val);

    start = &point1;

    reverse_list(start);

    return 0;
}



void reverse_list(node *start) {
    node *prev;
    node *nxt;
    int count = 0;
    while (start -> next != NULL){
    prev = start; //save adress of current node to be set as previous

    if (count == 0){
        start = start -> next; // move to next node
        prev -> next = NULL; //set first node next to NULL
    }

    else{
        start = nxt; //move to next node
    }
    if (start -> next == NULL){
        start -> next = prev;
        break;
    }

    nxt = start -> next; //save next node
  
    
    start -> next = prev; //set next node to previous node
    count++; //increase count
    

    }
    
    printf("\nlinked list reversed: ");

    while (start -> next != NULL){
        printf("current val: %d ", start -> val);
        start = start -> next;
    }

    printf("current val: %d ", start -> val);
    
    
    return;
}
