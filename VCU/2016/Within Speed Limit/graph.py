class town_node:
    def __init__(self, town):
        self.town = town
        self.highway = []
        self.time = []

    def add_highway(self, highway, time):
        self.highway.append(highway)
        self.time.append(time)
    
    def all_paths(self):
        upcoming = []
        passed = []

        for lanes in self.highway:
            upcoming.append(lanes)
        while len(upcoming) != 0:
            town = upcoming.pop()
            if town not in upcoming:
                print(town)
                passed.append(town)
                for lanes in town.highway:
                    upcoming.append(lanes)




town0 = town_node(0)

town1 = town_node(1)
town1.add_highway(town0, 1)
town0.add_highway(town1, 1)

town2 = town_node(2)
town2.add_highway(town0, 1)
town0.add_highway(town2, 1)


town3 = town_node(3)
town3.add_highway(town1, 1.67)
town1.add_highway(town3, 1.67)
town3.add_highway(town2, 1)
town2.add_highway(town3, 1)

town4 = town_node(4)
town4.add_highway(town2, 2)
town2.add_highway(town4, 2)

town5 = town_node(5)
town5.add_highway(town3, 1.5)
town3.add_highway(town5, 1.5)
town5.add_highway(town4, 1)
town4.add_highway(town5, 1)



town0.all_paths()

