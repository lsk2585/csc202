
blockSize = 3

block = [           #columns
                    [1,2,3],  #rows  
                    [4,5,6],
                    [7,8,9]
]
"""

block = [[1, 2, 3, 4],  #rows  
          [5, 6, 7, 8],
            [9,10,11,12],
              [13,14,15,16]]
"""

printLeft = blockSize**2 #elements left to print

row = 0
col = 0

RowTot = blockSize   #Total number of rows and total number of columns remaining
ColTot = blockSize

#blocksize = Scan("newline")


for i in range(blockSize**2):
                          #starting column  = 0
    while(col < ColTot and printLeft > 0): #print from top left to top right
            print(block[row][col])
            printLeft-=1
            col += 1
            if printLeft == 0:
                    break
    RowTot -= 1 #row total = 2
    row = RowTot - 1 #starting row index = 1
    col = ColTot - 1 #starting col index = 2

    while(row < RowTot + 1 and printLeft > 0): #print from top right to bottom right
            print(block[row][col])
            printLeft-=1
            row += 1
            if printLeft == 0:
                    break

    #print(row, col, ColTot, RowTot)
    ColTot -= 1 # column total is 2
    col  = ColTot - 1 # starting column index is 2 
    row -= 1

    while(col > (ColTot - blockSize) and printLeft > 0): #print from bottom right to bottom left
            print(block[row][col])
            printLeft-=1
            col -= 1
            if printLeft == 0:
                    break
    RowTot -= 1
    blockSize -= 1
    row = RowTot
    col += 1 
    while(row > (RowTot - blockSize) and printLeft > 0): #print from bottom left to top left
            print(block[row][col])
            printLeft-=1
            row -= 1
            if printLeft == 0:
                    break
    print (row, col, ColTot, RowTot, printLeft)
    col = ColTot - 1
    ColTot -= 1
    row += 1
    print (row, col, ColTot, RowTot, printLeft)
    
    #print(row, col, RowTot, ColTot)
    print("loop")
   



