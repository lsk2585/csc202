
int m = row 										# rows
int n = col										    # cols
int i,j = 0
int trow = 0, tcol = 0							    #rows and cols already printed
int rowNo = 0, colNo = 0
int count = row*col


def spiralPrint(mat, row, col):					
		
    												#count of total number of elements of the matrix to be printed

	while(true):															#recursive spiral printing till end of count
        for(colNo = tcol;colNo <= (n - tcol - 1);colNo++):					#printing from top left to top right
        
            #System.out.print("\n top left to top right");
            print(mat[rowNo][colNo]+" ");
            count-=1
            if(count == 0):
                return
        }
        colNo-=1
        tcol = colNo;

        for(rowNo = trow + 1; rowNo <= (m - trow - 1);rowNo++) 				#printing from top right to bottom right
        {
            #System.out.print("\n top right to bottom right");
            System.out.print(mat[rowNo][colNo]+" ");
            count--;
            if(count == 0)
            return;
        }
        rowNo--;
        trow = rowNo;

        for(colNo = tcol - 1;colNo >= (n - tcol - 1);colNo--) 				#printing from bottom right to bottom left
        {
            #System.out.print("\n bottom right to bottom left");
            System.out.print(mat[rowNo][colNo]+" ");
            count--;
            if(count == 0)
            return;
        }
        colNo++;
        tcol = colNo;

        for(rowNo = trow - 1; rowNo > (m - trow - 1);rowNo--)  				#printing from bottom left to top left
        {
            #System.out.print("\n bottom left to top left");
            System.out.print(mat[rowNo][colNo]+" ");
            count--;
            if(count == 0)
            return;
        }
        rowNo++;
        trow = rowNo;
        tcol++;
    }
}