import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class ProblemD {
	
	int length = 0;
	int[] minLeft;
	int[] maxRight;
	int[] numbers;
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String newLine = stdin.readLine();
		int size = Integer.parseInt(newLine);
		int[] test = new int[size];
		newLine = stdin.readLine();
		String[] parsedLine = newLine.split("\\s+");
		for(int i =0 ; i< size; i++){
			test[i] = Integer.parseInt(parsedLine[i]);
		}
		ProblemD planer = new ProblemD(test);
		planer.findMaxMin();
		System.out.println(planer.getSolution());
		
	}
	int getSolution(){
		int count = 0;
		for(int i = 0; i<length; i++){
			
		//		System.out.println(minLeft[i] +" "+i+" "+maxRight[i]+" = " +(minLeft[i] * maxRight[i]));
				
				count +=(minLeft[i] * maxRight[i]);
			
		}
		return count;
	}
	void findMaxMin(){
		for(int indx = 0; indx <length; indx++){
			
			int numMin = 0;
			
			int numMax = 0;
		
			for(int i = 0; i< indx;i++){
				if(numbers[i]< numbers[indx]){
					numMin++;
				}
				minLeft[indx] = numMin;
			}
			for(int i = indx; i< length;i++){
				if( numbers[i] > numbers[indx]){
					
					numMax++;
				}
				maxRight[indx] = numMax;
			}
		}
		
	}
	
	ProblemD(int[] nums){
		length = nums.length;
		numbers = nums;
		minLeft = new int[length];
		maxRight = new int[length];
		
	}
	
	
}
